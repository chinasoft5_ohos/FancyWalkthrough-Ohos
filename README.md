# FancyWalkthrough-Ohos

#### 项目介绍
- 项目名称：FancyWalkthrough-Ohos
- 所属系列：openharmony的第三方组件适配移植
- 功能：为应用创建酷炫的简介屏幕
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：release 2.1
#### 效果演示

![演示效果](https://images.gitee.com/uploads/images/2021/0623/150026_c9cd04b8_8695913.gif "截图.gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {

    repositories {

       maven {

              url 'https://s01.oss.sonatype.org/content/repositories/releases/'
             
            }

    }

}

 ```

2.在entry模块的build.gradle文件中，

 ```

 dependencies {

        implementation('com.gitee.chinasoft_ohos:fancywalkthroughlib:1.0.3')

    ......  

 }

 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.将仓库导入到本地仓库中

2.新增Ability继承FancyWalkthroughAbility,代码实例如下:
```
public class MainAbility extends FancyWalkthroughAbility {

    @Override
    public void initData() {

    }
}
```

3.然后创建FancyWalkthroughCard并进行相应的配置:
```
        FancyWalkthroughCard fancywalkthroughCard1 = new FancyWalkthroughCard("Find Restaurant", "Find the best restaurant in your neighborhood.", ResourceTable.Media_find_restaurant1);
        fancywalkthroughCard1.setBackgroundColor(getColor(ResourceTable.Color_white));
        fancywalkthroughCard1.setTitleColor(getColor(ResourceTable.Color_black));
        fancywalkthroughCard1.setDescriptionColor(getColor(ResourceTable.Color_black));
        fancywalkthroughCard1.setTitleTextSize(Utils.vpToPixels(getContext(), 10));
        fancywalkthroughCard1.setDescriptionTextSize(Utils.vpToPixels(getContext(), 8));
        fancywalkthroughCard1.setIconLayoutParams(300, 300, 0, 0, 0, 0);     
```

4.最后创建卡片列表并使用:
```
        List<FancyWalkthroughCard> pages = new ArrayList<>();
        pages.add(fancywalkthroughCard1);
        pages.add(fancywalkthroughCard2);
        pages.add(fancywalkthroughCard3);
        ...

        setOnboardPages(pages);
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.3
- 1.0.0
- 0.0.1-SNAPSHOT

### 版权和许可信息

```
Copyright 2018 Shashank Singhal

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
