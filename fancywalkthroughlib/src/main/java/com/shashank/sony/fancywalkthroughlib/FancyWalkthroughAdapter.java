package com.shashank.sony.fancywalkthroughlib;

import com.shashank.sony.fancywalkthroughlib.utils.ShadowTransformer;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.List;

/**
 * FancyWalkthroughAdapter
 *
 * @since 2021.07.12
 */
public class FancyWalkthroughAdapter extends PageSliderProvider implements ShadowTransformer.CardAdapter {
    List<Component> mList;

    /**
     * FancyWalkthroughAdapter instance
     *
     * @param list 适配器数据
     */
    public FancyWalkthroughAdapter(List<Component> list) {
        this.mList = list;
    }

    @Override
    public float getBaseElevation() {
        return 0;
    }

    @Override
    public Component getCardViewAt(int position) {
        FancyWalkthroughFragment fancyWalkthroughFragment = (FancyWalkthroughFragment) mList.get(position);
        return fancyWalkthroughFragment.getCardView();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        componentContainer.addComponent(mList.get(position));
        return mList.get(position);
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int position, Object object) {
        componentContainer.removeComponent(mList.get(position));
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }
}
