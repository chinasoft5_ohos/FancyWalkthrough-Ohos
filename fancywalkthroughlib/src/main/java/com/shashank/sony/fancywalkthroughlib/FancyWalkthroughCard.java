package com.shashank.sony.fancywalkthroughlib;

import ohos.media.image.PixelMap;

/**
 * FancyWalkthroughCard
 *
 * @since 2021.07.12
 */
public class FancyWalkthroughCard {
    private String title;
    private String description;
    private PixelMap imageResource;
    private int titleResourceId;
    private int descriptionResourceId;
    private int imageResourceId;
    private int titleColor;
    private int descriptionColor;
    private int backgroundColor;

    private int titleTextSize;
    private int descriptionTextSize;
    private int iconWidth;
    private int iconHeight;
    private int marginTop;
    private int marginLeft;
    private int marginRight;
    private int marginBottom;

    /**
     * FancyWalkthroughCard instacne
     *
     * @param title 标题
     * @param description 描述
     */
    public FancyWalkthroughCard(String title, String description) {
        this.title = title;
        this.description = description;
    }

    /**
     * FancyWalkthroughCard instacne
     *
     * @param title 标题id
     * @param description 描述id
     */
    public FancyWalkthroughCard(int title, int description) {
        this.titleResourceId = title;
        this.descriptionResourceId = description;
    }

    /**
     * FancyWalkthroughCard instacne
     *
     * @param title 标题
     * @param description 描述
     * @param imageResourceId 图片id
     */
    public FancyWalkthroughCard(String title, String description, int imageResourceId) {
        this.title = title;
        this.description = description;
        this.imageResourceId = imageResourceId;
    }

    /**
     * FancyWalkthroughCard instacne
     *
     * @param title 标题
     * @param description 描述
     * @param imageResource 图片
     */
    public FancyWalkthroughCard(String title, String description, PixelMap imageResource) {
        this.title = title;
        this.description = description;
        this.imageResource = imageResource;
    }

    /**
     * FancyWalkthroughCard instacne
     *
     * @param title 标题id
     * @param description 描述id
     * @param imageResourceId 图片id
     */
    public FancyWalkthroughCard(int title, int description, int imageResourceId) {
        this.titleResourceId = title;
        this.descriptionResourceId = description;
        this.imageResourceId = imageResourceId;
    }

    /**
     * FancyWalkthroughCard instacne
     *
     * @param title 标题id
     * @param description 描述id
     * @param imageResource 图片
     */
    public FancyWalkthroughCard(int title, int description, PixelMap imageResource) {
        this.titleResourceId = title;
        this.descriptionResourceId = description;
        this.imageResource = imageResource;
    }

    /**
     * getTitle
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * getTitleResourceId
     *
     * @return titleResourceId
     */
    public int getTitleResourceId() {
        return titleResourceId;
    }

    /**
     * getDescription
     *
     * @return 描述
     */
    public String getDescription() {
        return description;
    }

    /**
     * getDescriptionResourceId
     *
     * @return descriptionResourceId
     */
    public int getDescriptionResourceId() {
        return descriptionResourceId;
    }

    /**
     * getTitleColor
     *
     * @return titleColor
     */
    public int getTitleColor() {
        return titleColor;
    }

    /**
     * getImageResource
     *
     * @return PixelMap
     */
    public PixelMap getImageResource() {
        return imageResource;
    }

    /**
     * getDescriptionColor
     *
     * @return descriptionColor
     */
    public int getDescriptionColor() {
        return descriptionColor;
    }

    /**
     * setTitleColor
     *
     * @param color title颜色
     */
    public void setTitleColor(int color) {
        this.titleColor = color;
    }

    /**
     * setDescriptionColor
     *
     * @param color description颜色
     */
    public void setDescriptionColor(int color) {
        this.descriptionColor = color;
    }

    /**
     * setImageResourceId
     *
     * @param imageResourceId 图片id
     */
    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    /**
     * getImageResourceId
     *
     * @return imageResourceId
     */
    public int getImageResourceId() {
        return imageResourceId;
    }

    /**
     * getTitleTextSize
     *
     * @return titleTextSize
     */
    public int getTitleTextSize() {
        return titleTextSize;
    }

    /**
     * setTitleTextSize
     *
     * @param titleTextSize titleTextSize
     */
    public void setTitleTextSize(int titleTextSize) {
        this.titleTextSize = titleTextSize;
    }

    /**
     * getDescriptionTextSize
     *
     * @return descriptionTextSize
     */
    public int getDescriptionTextSize() {
        return descriptionTextSize;
    }

    /**
     * setDescriptionTextSize
     *
     * @param descriptionTextSize 字体大小
     */
    public void setDescriptionTextSize(int descriptionTextSize) {
        this.descriptionTextSize = descriptionTextSize;
    }

    /**
     * getBackgroundColor
     *
     * @return backgroundColor
     */
    public int getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * setBackgroundColor
     *
     * @param backgroundColor 背景色
     */
    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * getIconWidth
     *
     * @return iconWidth
     */
    public int getIconWidth() {
        return iconWidth;
    }

    /**
     * setIconLayoutParams
     *
     * @param iconWidth 图片宽
     * @param iconHeight 图片高
     * @param marginTop 顶部距离
     * @param marginLeft 左边距离
     * @param marginRight 右边距离
     * @param marginBottom 底部距离
     */
    public void setIconLayoutParams(int iconWidth, int iconHeight,
            int marginTop, int marginLeft, int marginRight, int marginBottom) {
        this.iconWidth = iconWidth;
        this.iconHeight = iconHeight;
        this.marginLeft = marginLeft;
        this.marginRight = marginRight;
        this.marginTop = marginTop;
        this.marginBottom = marginBottom;
    }

    /**
     * getIconHeight
     *
     * @return iconHeight
     */
    public int getIconHeight() {
        return iconHeight;
    }

    /**
     * getMarginTop
     *
     * @return marginTop
     */
    public int getMarginTop() {
        return marginTop;
    }

    /**
     * getMarginLeft
     *
     * @return marginLeft
     */
    public int getMarginLeft() {
        return marginLeft;
    }

    /**
     * getMarginRight
     *
     * @return marginRight
     */
    public int getMarginRight() {
        return marginRight;
    }

    /**
     * getMarginBottom
     *
     * @return marginBottom
     */
    public int getMarginBottom() {
        return marginBottom;
    }
}
