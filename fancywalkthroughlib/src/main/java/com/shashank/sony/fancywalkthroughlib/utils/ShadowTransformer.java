package com.shashank.sony.fancywalkthroughlib.utils;


import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;


/**
 * Created by Rahul Juneja on 30-08-2016.
 */
public class ShadowTransformer implements PageSlider.PageChangedListener {

    private PageSlider mViewPager;
    private CardAdapter mAdapter;
    private boolean mScalingEnabled;

    @Override
    public void onPageSliding(int position, float positionOffset, int positionOffsetPixels) {
        int realCurrentPosition;
        int nextPosition;
        float baseElevation = mAdapter.getBaseElevation();
        float realOffset;
        boolean goingLeft = positionOffsetPixels < 0;
        // If we're going backwards, onPageScrolled receives the last position
        // instead of the current one
        if (goingLeft) {
            realCurrentPosition = position;
            nextPosition = position - 1;
        } else {
            nextPosition = position + 1;
            realCurrentPosition = position;
        }
        realOffset = positionOffset;

        // Avoid crash on overscroll
        if (nextPosition > mAdapter.getCount() - 1
                || realCurrentPosition > mAdapter.getCount() - 1) {
            return;
        }

        Component currentCard = mAdapter.getCardViewAt(realCurrentPosition);

        // This might be null if a fragment is being used
        // and the views weren't created yet
        if (currentCard != null) {
            if (mScalingEnabled) {
                currentCard.setScaleX((float) (1 + 0.1 * (1 - realOffset)));
                currentCard.setScaleY((float) (1 + 0.1 * (1 - realOffset)));
            }
//            currentCard.setCardElevation((baseElevation + baseElevation
//                    * (CardAdapter.MAX_ELEVATION_FACTOR - 1) * (1 - realOffset)));
        }

        Component nextCard = mAdapter.getCardViewAt(nextPosition);

        // We might be scrolling fast enough so that the next (or previous) card
        // was already destroyed or a fragment might not have been created yet
        if (nextCard != null) {
            if (mScalingEnabled) {
                nextCard.setScaleX((float) (1 + 0.1 * (realOffset)));
                nextCard.setScaleY((float) (1 + 0.1 * (realOffset)));
            }
//            nextCard.setCardElevation((baseElevation + baseElevation
//                    * (CardAdapter.MAX_ELEVATION_FACTOR - 1) * (realOffset)));

        }
    }

    @Override
    public void onPageSlideStateChanged(int i) {

    }

    @Override
    public void onPageChosen(int i) {

    }

    public interface CardAdapter {

        int MAX_ELEVATION_FACTOR = 6;

        float getBaseElevation();

        Component getCardViewAt(int position);

        int getCount();
    }

    public ShadowTransformer(PageSlider viewPager, CardAdapter adapter) {
        mViewPager = viewPager;
        viewPager.addPageChangedListener(this);
        mAdapter = adapter;
    }

    public void enableScaling(boolean enable) {
        if (mScalingEnabled && !enable) {
            // shrink main card
            Component currentCard = mAdapter.getCardViewAt(mViewPager.getCurrentPage());
            if (currentCard != null) {
                currentCard.setScaleX(1);
                currentCard.setScaleY(1);
            }
        } else if (!mScalingEnabled && enable) {
            // grow main card
            Component currentCard = mAdapter.getCardViewAt(mViewPager.getCurrentPage());
            if (currentCard != null) {
                currentCard.setScaleX(1.1f);
                currentCard.setScaleY(1.1f);
            }
        }

        mScalingEnabled = enable;
    }
}
