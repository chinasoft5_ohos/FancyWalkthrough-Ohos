/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.shashank.sony.fancywalkthroughlib.utils;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Utils
 *
 * @since 2021.07.12
 */
public class Utils {
    /**
     * vp转px
     *
     * @param context 上下文
     * @param vp vp值
     * @return px值
     */
    public static int vpToPixels(Context context, int vp) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float dpi = display.getAttributes().densityPixels;
        return (int) (vp * dpi + 0.5d);
    }

    /**
     * red
     *
     * @param color 颜色值
     * @return 提取的颜色值
     */
    public static int red(int color) {
        return (color >> 16) & 0xFF;
    }

    /**
     * green
     *
     * @param color 颜色值
     * @return 提取的颜色值
     */
    public static int green(int color) {
        return (color >> 8) & 0xFF;
    }

    /**
     * blue
     *
     * @param color 颜色值
     * @return 提取的颜色值
     */
    public static int blue(int color) {
        return color & 0xFF;
    }
}
