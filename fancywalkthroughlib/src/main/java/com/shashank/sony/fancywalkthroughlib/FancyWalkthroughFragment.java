package com.shashank.sony.fancywalkthroughlib;

import com.shashank.sony.fancywalkthroughlib.utils.Utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * FancyWalkthroughFragment
 *
 * @since 2021.07.12
 */
public class FancyWalkthroughFragment extends DirectionalLayout {
    private int backgroundColor;
    private int imageResId;

    private Component mBottomLayout;
    private Component view1;
    private FancyWalkthroughCard mPage;
    private Image ivOnboarderImage;
    private Text tvOnboarderTitle;
    private Text tvOnboarderDescription;
    private int iconHeight;
    private int iconWidth;
    private int marginTop;
    private int marginBottom;
    private int marginLeft;
    private int marginRight;

    /**
     * FancyWalkthroughFragment instance
     *
     * @param context  上下文
     * @param page     对象
     * @param fontType 字体
     */
    public FancyWalkthroughFragment(Context context, FancyWalkthroughCard page, Font fontType) {
        super(context);
        bindView(context, page, fontType);
    }

    private void bindView(Context context, FancyWalkthroughCard page, Font fontType) {
        if (page != null) {
            this.mPage = page;
        } else {
            throw new IllegalArgumentException("FancyWalkthroughCard must not be null.");
        }

        initView();
        initData(fontType);

        ShapeElement shapeElement = new ShapeElement();
        int topRadius = Utils.vpToPixels(context, 40);
        float[] radius = {topRadius, topRadius, topRadius, topRadius, 0, 0, 0, 0};
        shapeElement.setCornerRadiiArray(radius);
        if (backgroundColor == 0) {
            backgroundColor = context.getColor(ResourceTable.Color_white);
        }
        backgroundColor = adjustAlpha(backgroundColor, 0.6f);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(backgroundColor));
        view1.setBackground(shapeElement);

        if (imageResId != 0) {
            ivOnboarderImage.setPixelMap(imageResId);
        }
        if (iconWidth == 0 || iconHeight == 0) {
            iconWidth = Utils.vpToPixels(context, 128);
            iconHeight = Utils.vpToPixels(context, 128);
        }
        if (marginTop == 0) {
            marginTop = Utils.vpToPixels(context, 80);
        }
        StackLayout.LayoutConfig layoutConfigs = new StackLayout.LayoutConfig(iconWidth, iconHeight);
        layoutConfigs.alignment = LayoutAlignment.HORIZONTAL_CENTER;
        layoutConfigs.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        ivOnboarderImage.setLayoutConfig(layoutConfigs);
    }

    private void initView() {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        Component view = layoutScatter.parse(ResourceTable.Layout_fragment_ahoy, null, false);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        view.setLayoutConfig(layoutConfig);
        addComponent(view);

        //  view.setWidth(DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().width);
        ivOnboarderImage = (Image) view.findComponentById(ResourceTable.Id_iv_image);
        tvOnboarderTitle = (Text) view.findComponentById(ResourceTable.Id_tv_title);
        tvOnboarderDescription = (Text) view.findComponentById(ResourceTable.Id_tv_description);
        mBottomLayout = view.findComponentById(ResourceTable.Id_bottom_layout);
        view1 = view.findComponentById(ResourceTable.Id_view1);

    }

    private void initData(Font fontType) {
        String title = mPage.getTitle();
        int titleResId = mPage.getTitleResourceId();
        int titleColor = mPage.getTitleColor();
        int titleTextSize = mPage.getTitleTextSize();
        String description = mPage.getDescription();
        int descriptionResId = mPage.getDescriptionResourceId();
        int descriptionColor = mPage.getDescriptionColor();
        int descriptionTextSize = mPage.getDescriptionTextSize();
        imageResId = mPage.getImageResourceId();
        backgroundColor = mPage.getBackgroundColor();
        iconWidth = mPage.getIconWidth();
        iconHeight = mPage.getIconHeight();
        marginTop = mPage.getMarginTop();
        marginBottom = mPage.getMarginBottom();
        marginLeft = mPage.getMarginLeft();
        marginRight = mPage.getMarginRight();

        if (title != null) {
            tvOnboarderTitle.setText(title);
        }
        if (titleResId != 0) {
            tvOnboarderTitle.setText(getContext().getString(titleResId));
        }
        if (titleColor != 0) {
            tvOnboarderTitle.setTextColor(new Color(titleColor));
        }
        if (titleTextSize != 0) {
            tvOnboarderTitle.setTextSize(titleTextSize);
        }

        if (description != null) {
            tvOnboarderDescription.setText(description);
        }
        if (descriptionResId != 0) {
            tvOnboarderDescription.setText(getContext().getString(descriptionResId));
        }
        if (descriptionColor != 0) {
            tvOnboarderDescription.setTextColor(new Color(descriptionColor));
        }
        if (descriptionTextSize != 0) {
            tvOnboarderDescription.setTextSize(descriptionTextSize);
        }

        if (fontType != null) {
            tvOnboarderTitle.setFont(fontType);
            tvOnboarderDescription.setFont(fontType);
        }
    }

    private int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Utils.red(color);
        int green = Utils.green(color);
        int blue = Utils.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public Component getCardView() {
        return mBottomLayout;
    }

}
