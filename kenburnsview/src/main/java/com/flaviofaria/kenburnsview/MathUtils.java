/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.flaviofaria.kenburnsview;

import ohos.agp.utils.RectFloat;

/**
 * MathUtils
 *
 * @since 2021.07.12
 */
public final class MathUtils {
    /**
     * Truncates a float number {@code f} to {@code decimalPlaces}.
     *
     * @param truncateNumber the number to be truncated.
     * @param decimalPlaces the amount of decimals that {@code f}
     * will be truncated to.
     * @return a truncated representation of {@code f}.
     */
    protected static float truncate(float truncateNumber, int decimalPlaces) {
        float decimalShift = (float) Math.pow(10, decimalPlaces);
        return Math.round(truncateNumber * decimalShift) / decimalShift;
    }

    /**
     * Checks whether two {@link RectFloat} have the same aspect ratio.
     *
     * @param r1 the first rect.
     * @param r2  the second rect.
     * @return {@code true} if both rectangles have the same aspect ratio,
     * {@code false} otherwise.
     */
    protected static boolean haveSameAspectRatio(RectFloat r1, RectFloat r2) {
        /**
         * Reduces precision to avoid problems when comparing aspect ratios.
         */
        float srcRectRatio = MathUtils.truncate(MathUtils.getRectRatio(r1), 3);
        float dstRectRatio = MathUtils.truncate(MathUtils.getRectRatio(r2), 3);

        /**
         * Compares aspect ratios that allows for a tolerance range of [0, 0.01]
         */
        return (Math.abs(srcRectRatio - dstRectRatio) <= 0.01f);
    }

    /**
     * Computes the aspect ratio of a given rect.
     *
     * @param rect the rect to have its aspect ratio computed.
     * @return the rect aspect ratio.
     */
    protected static float getRectRatio(RectFloat rect) {
        return rect.getWidth() / rect.getHeight();
    }
}
