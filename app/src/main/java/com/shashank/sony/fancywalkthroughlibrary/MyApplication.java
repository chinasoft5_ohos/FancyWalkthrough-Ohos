package com.shashank.sony.fancywalkthroughlibrary;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @since 2021.07.12
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
