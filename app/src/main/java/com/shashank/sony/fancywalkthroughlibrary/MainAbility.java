package com.shashank.sony.fancywalkthroughlibrary;

import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughAbility;
import com.shashank.sony.fancywalkthroughlib.FancyWalkthroughCard;
import com.shashank.sony.fancywalkthroughlib.utils.Utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;

import java.util.ArrayList;
import java.util.List;

/**
 * MainAbility
 *
 * @since 2021.07.12
 */
public class MainAbility extends FancyWalkthroughAbility {
    /**
     * initData 初始化数据
     */
    @Override
    public void initData() {
        FancyWalkthroughCard fancywalkthroughCard1 = new FancyWalkthroughCard(
                "Find Restaurant", "Find the best restaurant in your neighborhood.",
                ResourceTable.Media_find_restaurant1);
        FancyWalkthroughCard fancywalkthroughCard2 = new FancyWalkthroughCard(
                "Pick the best", "Pick the right place using trusted ratings and reviews.",
                ResourceTable.Media_pickthebest);
        FancyWalkthroughCard fancywalkthroughCard3 = new FancyWalkthroughCard(
                "Choose your meal", "Easily find the type of food you're craving.",
                ResourceTable.Media_chooseurmeal);
        FancyWalkthroughCard fancywalkthroughCard4 = new FancyWalkthroughCard(
                "Meal is on the way", "Get ready and comfortable while our biker bring your meal at your door.",
                ResourceTable.Media_mealisonway);

        fancywalkthroughCard1.setBackgroundColor(getColor(ResourceTable.Color_white));
        fancywalkthroughCard1.setIconLayoutParams(300, 300, 0, 0, 0, 0);
        fancywalkthroughCard2.setBackgroundColor(getColor(ResourceTable.Color_white));
        fancywalkthroughCard2.setIconLayoutParams(300, 300, 0, 0, 0, 0);
        fancywalkthroughCard3.setBackgroundColor(getColor(ResourceTable.Color_white));
        fancywalkthroughCard3.setIconLayoutParams(300, 300, 0, 0, 0, 0);
        fancywalkthroughCard4.setBackgroundColor(getColor(ResourceTable.Color_white));
        fancywalkthroughCard4.setIconLayoutParams(300, 300, 0, 0, 0, 0);

        List<FancyWalkthroughCard> pages = new ArrayList<>();
        pages.add(fancywalkthroughCard1);
        pages.add(fancywalkthroughCard2);
        pages.add(fancywalkthroughCard3);
        pages.add(fancywalkthroughCard4);
        for (FancyWalkthroughCard page : pages) {
            page.setTitleColor(getColor(ResourceTable.Color_black));
            page.setDescriptionColor(getColor(ResourceTable.Color_black));
        }

        setFinishButtonTitle("Get Started");
        setActiveIndicatorColor(getColor(ResourceTable.Color_white));
        showNavigationControls(true);
        setColorBackground(getColor(ResourceTable.Color_colorGreen));
        setInactiveIndicatorColor(getColor(ResourceTable.Color_grey_600));
        setActiveIndicatorColor(getColor(ResourceTable.Color_colorGreen));
        setOnboardPages(pages);
    }

    /**
     * onFinishButtonPressed finish按钮被点击
     */
    @Override
    public void onFinishButtonPressed() {
        DirectionalLayout toastLayout = new DirectionalLayout(getContext());
        DirectionalLayout.LayoutConfig toastLayoutConfig =
                new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastLayout.setAlignment(LayoutAlignment.BOTTOM | LayoutAlignment.HORIZONTAL_CENTER);
        toastLayout.setLayoutConfig(toastLayoutConfig);

        DirectionalLayout.LayoutConfig textConfig =
                new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        Text text = new Text(getContext());
        text.setText("Finish Pressed");
        text.setTextColor(new Color(getColor(ResourceTable.Color_black)));
        text.setPadding(Utils.vpToPixels(getContext(), 16), Utils.vpToPixels(getContext(), 10),
                Utils.vpToPixels(getContext(), 16), Utils.vpToPixels(getContext(), 10));
        text.setTextSize(Utils.vpToPixels(getContext(), 12));
        text.setBackground(getElementByColorRadius(Color.getIntColor("#99FFFFFF"), Utils.vpToPixels(getContext(), 20)));
        text.setLayoutConfig(textConfig);
        text.setTextAlignment(TextAlignment.CENTER);
        toastLayout.addComponent(text);
        ToastDialog toastDialog = new ToastDialog(getContext());

        toastLayout.setMarginBottom(150);
        toastDialog.setComponent(toastLayout);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setTransparent(true);
        toastDialog.setDuration(1000);
        toastDialog.show();
    }

    private Element getElementByColorRadius(int color, float radius) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(0);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        shapeElement.setCornerRadius(radius);
        return shapeElement;
    }
}
