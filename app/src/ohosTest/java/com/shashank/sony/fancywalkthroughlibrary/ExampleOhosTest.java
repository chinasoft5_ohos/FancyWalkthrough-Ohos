package com.shashank.sony.fancywalkthroughlibrary;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * ExampleOhosTest
 *
 * @since : 2021.07.12
 */
public class ExampleOhosTest {
    /**
     * test bundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.shashank.sony.fancywalkthroughlibrary", actualBundleName);
    }
}