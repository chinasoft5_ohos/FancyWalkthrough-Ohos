package com.shashank.sony.fancywalkthroughlibrary;

import com.shashank.sony.fancywalkthroughlib.utils.Utils;

import org.junit.Assert;
import org.junit.Test;

/**
 * UtilsTest
 *
 * @since : 2021.06.24
 */
public class UtilsTest {
    /**
     * test red
     */
    @Test
    public void red() {
        int color = -1;
        int redColor = Utils.red(color);
        Assert.assertEquals(255, redColor);
    }

    /**
     * test green
     */
    @Test
    public void green() {
        int color = -1;
        int greenColor = Utils.green(color);
        Assert.assertEquals(255, greenColor);
    }

    /**
     * test blue
     */
    @Test
    public void blue() {
        int color = -1;
        int blueColor = Utils.blue(color);
        Assert.assertEquals(255, blueColor);
    }
}